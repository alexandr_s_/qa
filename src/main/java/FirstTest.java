import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

/**
 * @author Alexandr Sirchenko
 */
public class FirstTest {
    static WebDriver driver;

    @BeforeClass
    public static void startUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\D\\code\\selenium\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void firstTest() {
        driver.get("https://reports.spd-ukraine.com");
        String windowTitle = driver.getTitle();
        assertTrue("Wrong window title", windowTitle.equalsIgnoreCase("Login page"));
    }

    @Test
    public void shouldNotAuthenticate() {
        driver.get("http://qa.reports.spd-ukraine.com");
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));
        WebElement signInButton = driver.findElement(By.xpath("//*[@id='formLogin']//button[@type='submit']"));
        login.sendKeys("wrongemail.@gmail.com");
        password.sendKeys("wrongpassword");
        signInButton.submit();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("loginMessage")));
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }
}
